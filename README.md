# About /Requirements
This CLI monitors for the Event.SC2BANK file that is written as a Team 5 observer
in the ZCCE Arcade Game on SC2. This will send data over to our server to record
matches.

- Requires Python 3.6
- Make sure you can type in #check-ins channel on Discord

# Installation

### Clone the repository

`git clone https://gitlab.com/dfitzpatrick/matchcli.git`

### Change into the directory
`cd matchcli`

### Install Pipenv
`pip install pipenv`

`pipenv install`

### Add two Environment Variables to your Computer
#### `BANK_DIR`
This is the file path to the Bank directory that contains ZCCE bank files.
ZCCE is generally found in 1-S2-1-4632373

#### `DISCORD_ID`
Put your discord id in here. This is for convenience

### Run the client
`pipenv run client.py`


In Discord, start a new queue session in #check-ins channel.
!q prepare

The client asks for your discord id. This will show on the embed that pops up in
Discord.

Enter league and season. Just hit enter without entering anything to not set.

Time to rock!
