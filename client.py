import asyncio
import json
import msvcrt
import os
import re
import textwrap
import win32file
import xml.etree.ElementTree as ET

import PySimpleGUI
import pywintypes
import websockets
import typing


from layouts import layout, stop_listening_yesno_popup, recovery_yesno_popup
from layouts import match_draw_popup
from pyupdater.client import Client
from client_config import ClientConfig

APP_NAME = 'matchcli'
APP_VERSION = '0.1.1'
LAST_EVENT = {}

class OpenWithoutLock:
    """
    This class acts as a context manager to safely open a file in Windows
    without locking the file.
    From https://www.thepythoncorner.com/2016/10/python-how-to-open-a-file-on-windows-without-locking-it/
    """

    def __init__(self, filename, mode):
        self.filename = filename
        self.mode = mode

    def __enter__(self):
        handle = win32file.CreateFile(
            self.filename,
            win32file.GENERIC_READ,
            win32file.FILE_SHARE_DELETE |
            win32file.FILE_SHARE_READ |
            win32file.FILE_SHARE_WRITE,
            None,
            win32file.OPEN_EXISTING,
            0,
            None
        )
        detached_handle = handle.Detach()
        file_descriptor = msvcrt.open_osfhandle(
            detached_handle, os.O_RDONLY
        )
        self.open_file = open(file_descriptor)
        return self.open_file

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_tb:
            print(exc_tb)
        self.open_file.close()

def get_uri(streamer_id, local=False) -> str:
    """
    Returns a formatted web socket uri that will have messages transmitted
    from the REDIS server.

    Parameters
    ----------
    streamer_id: :class: `int`
        The discord/streamer id of the person to use.
    local: :class: `bool`
        Return a localhost formatted uri. Defaults to False.

    Returns
    -------
    The uri in :class: `str` format.

    """

    uri = 'ws://www.zcleagues.com/ws/obs/{streamer_id}/'
    if local:
        uri = 'ws://localhost:8000/ws/obs/{streamer_id}/'
    uri = uri.format(streamer_id=streamer_id)
    return uri

def set_streamer(streamer_id: int) -> None:
    """
    Updates the meta information for the listener with a new streamer id.
    Parameters
    ----------
    streamer_id: :class: `int`
        The discord/streamer id of the person to use.

    Returns
    -------
    None
    """
    global settings
    settings['meta']['streamer_id'] = streamer_id
    settings['meta']['uri'] = get_uri(streamer_id)

def get_game_id() -> typing.Optional[str]:
    """
    Opens the current SC2Bank file and looks to see if it can get the
    current game id from the game. This is the id that is associated with
    each SECTION tag in the bank file. Keep in mind that this data is stored
    as a string.

    Returns
    -------
    Optional[str]
    """
    try:
        event_file = f"{settings['bank_dir']}/{EVENT_BANK}"
        with OpenWithoutLock(event_file, 'r') as bank:
            data = bank.read()
            tree = ET.fromstring(data)
            section: ET.Element = tree.find("Section")
            game_id = section.attrib.get('name')
            return game_id
    except pywintypes.error as e:
        print("Could not open Bank FIle")
        return

def prepare_chat_xml():
    global chat_filename
    root = ET.Element("Bank")
    root.set("version", "1")
    section = ET.SubElement(root, "Section")
    hash_value = hash(ET.tostring(root))
    section.set("name", str(abs(hash_value)))
    #section.set("name", "1")
    tree = ET.ElementTree(root)
    tree.write(chat_filename, encoding='utf-8', xml_declaration=True)



def write_chat_xml(k, v):
    global chat_filename
    global settings

    tree = ET.parse(chat_filename)
    root = tree.getroot()
    section: ET.Element = tree.find("Section")
    number_children = len(section.getchildren())
    print(f"number children: {number_children}")
    n = (number_children // 2) + 1
    stored_n = settings['meta'].get('twitch_chat_n')
    if stored_n:
        n = stored_n + 1

    print(f"n is {n}")
    name_key = ET.SubElement(section, "Key")
    name_key.set("name", f"{n}n")
    name_value = ET.SubElement(name_key, "Value")
    name_value.set("text", k)

    message_key = ET.SubElement(section, "Key")
    message_key.set("name", f"{n}v")

    message_value = ET.SubElement(message_key, "Value")
    message_value.set("text", v)


    hash_value = hash(ET.tostring(root))
    section.set('name', str(abs(hash_value)))
    #section.set("name", "1")

    settings['meta']['twitch_chat_n'] = n
    write_settings(settings)
    tree.write(chat_filename)

async def check_for_new_game() -> None:
    """
    |coro|

    This function will block until it detects a new game id that loads into the
    file. If no file exists, it will wait until it is able to retrieve it after
    creation.

    Returns
    -------
    None
    """
    global settings
    print("Checking for game..")
    while True:
        game_id = get_game_id()
        if game_id != settings['meta'].get('game_id') and game_id is not None:
            settings['meta']['game_id'] = game_id
            return
        await asyncio.sleep(5)

async def consumer():
    """
    |coro|

    Consumes messages received from the channel.

    Returns
    -------
    None
    """
    ws: websockets.WebSocketClientProtocol = await websockets.connect((settings['meta']['uri']))
    add_to_log("Consumer Running...")
    while True:
        #add_to_log("Checking for event")
        #print("checking")
        event = await ws.recv()
        #print("Received value")
        #add_to_log("Event Received from Consumer")
        try:
            event = json.loads(event)
            if event['type'] == 'twitch':
                add_to_log("New Twitch Message")
                await twitch(event)
        except json.JSONDecodeError:
            pass

async def twitch(event: typing.Dict[str, typing.Any]):
    if event.get('subtype') == 'message':
        message = event.get('payload')
        if not message:
            return
        if not os.path.exists(chat_filename):
            prepare_chat_xml()
        write_chat_xml(message['author'], message['content'])

async def producer(recover=False, recover_resume=True):
    """
    |coro|

    This producer consumes events from the bank file it is monitoring and
    continuously fires off the result to the REDIS uri for that streamer.

    This will terminate under two conditions:
        1. The file no longer exists
        2. A "match_end" event is processed.

    Parameters
    ----------
    recover: :class: `bool`
        Defaults to False. True indicates that the producer is running in recovery
        mode. This is the result of a refactor. In honesty, it may not be needed
        any longer.

    recover_resume: :class: `bool`
        Defaults to True. This flag specifies that while recovering a file, it
        should resume from the last written key in the settings file.

        Setting this to False will force the recovery to read from key 0.

    Returns
    -------
    None
    """
    global LAST_EVENT
    global window
    next_key = 0
    if recover:
        if recover_resume:
            next_key = settings['meta'].get('key', 0)
        else:
            next_key = 0
            del settings['meta']['twitch_chat_n']
        if not isinstance(next_key, int):
            next_key = 0

    add_to_log(f"Now listening for key: {next_key}")
    event_file = f"{settings['bank_dir']}/{EVENT_BANK}"

    while True:
        if task:
            if task.cancelled():
                break
        try:
            with OpenWithoutLock(event_file, 'r') as bank:
                data = bank.read()
        except pywintypes.error as e:
            # File does not exist
            return
        try:
            tree = ET.fromstring(data)
            section: ET.Element = tree.find("Section")
            game_id = section.attrib.get('name')
            settings['meta']['game_id'] = game_id
            search = f"Key[@name='{next_key}']/Value"
            event = section.find(search)
            if event is not None:
                jdata = event.attrib.get('text', {})
                jdata = jdata.replace('`', '"')
                pattern = re.compile(r',(?=\s+[]}])')
                jdata = re.sub(pattern, "", jdata)

                # Validate
                try:
                    jdata = json.loads(jdata)
                    jdata['game_id'] = game_id
                    jdata['season'] = settings['meta'].get('season')
                    jdata['league'] = settings['meta'].get('league')
                    jdata['streamer_id'] = settings['discord_id']

                    if next_key == 0 and jdata.get('type') != 'match_start':
                        print("First key not Match Start...ending")
                        break
                except json.JSONDecodeError:
                    add_to_log(f"WARNING: NOT VALID JSON {jdata}")
                    next_key += 1
                    continue
                LAST_EVENT = jdata
                await send_event(settings['meta']['uri'], json.dumps(jdata))
                if jdata.get('type') == 'match_start':
                    prepare_chat_xml()
                    settings['meta']['twitch_chat_n'] = 0
                    window.Element('draw').Update(disabled=False)

                if jdata.get('type') == 'match_end':
                    if os.path.exists(chat_filename):
                        prepare_chat_xml()
                    settings['meta']['twitch_chat_n'] = 0
                    window.Element('draw').Update(disabled=True)
                    break
                #if jdata.get('type') == 'match_start':
                #    prepare_chat_xml()
                next_key += 1
                settings['meta']['key'] = next_key
                add_to_log(f"Waiting for next key #{next_key}....")
                write_settings(settings)
        except ET.ParseError as e:
            add_to_log(f"Unknown Parser Error: {e}")
        await asyncio.sleep(1)

async def send_event(uri: str, event: str) -> None:
    """
    This will transmit data over the websocket to the REDIS server.
    Sometimes the server throws a random 500 server error. This appears to be
    a bug in a dependency on Django Channel Redis implementation. This will
    keep sending until it does.

    Parameters
    ----------
    uri: :class: `str`
        The uri to use when connecting
    event: class: `str`
        The json string of the event
    Returns
    -------
    None
    """
    while True:
        try:
            async with websockets.connect(uri) as ws:
                add_to_log(f"Sending: {event}")
                await ws.send(event)
                break
        except Exception as e:
            add_to_log(f"Retrying: {e}")
            await asyncio.sleep(1)
            continue

async def test_connection(uri: str) -> bool:
    """
    Tests if a websocket connection to a specific uri is successful.
    Parameters
    ----------
    uri: :class: `str`
        The uri to connect to.

    Returns
    -------
    bool
    """
    try:
        async with websockets.connect(uri) as ws:
            return True
    except:
        return False

def get_settings() -> typing.Dict[str, typing.Any]:
    """
    Gets the settings from the persisted file or creates a new dict with
    defaults.

    Returns
    -------
    Dict[str, Any]
    """
    meta_default = {
        'season': None,
        'league': None,
        'streamer_id': None,
        'game_id': None,
        'uri': None,
        'key': 0,
    }
    try:
        with open("settings.json", 'r') as f:
            local_settings = json.load(f)
    except (FileNotFoundError, PermissionError):
        local_settings = {}

    if 'meta' not in local_settings.keys():
        local_settings['meta'] = meta_default
    return local_settings

def get_bank_path(window: PySimpleGUI.Window,
                  values: typing.Dict[str, typing.Any]) -> str:
    """
    Returns what the bank path is as was set by the user in the GUI.
    Parameters
    ----------
    window:
    values

    Returns
    -------
    :class: `str`
    """
    bank_dir = (
        values.get(
            'browse',
            window.Element('bank_dir').DisplayText
        )
    ) or window.Element('bank_dir').DisplayText
    return bank_dir


def write_settings(settings: typing.Dict[str, typing.Any]) -> None:
    """
    Persists the current settings and state to disk. This is used as initial
    on startup in between program launches.

    Parameters
    ----------
    settings: :class: `dict`
        The settings dictionary to persist.

    Returns
    -------
    None
    """
    try:
        with open("settings.json", "w") as f:
            json.dump(settings, f)
    except PermissionError:
        add_to_log("Could not write settings. Permissions error.")

def add_to_log(text: str) -> None:
    """
    Adds a line of text to the GUI list box that serves as the logger.

    Parameters
    ----------
    text: :class: `str`
        The text to add

    Returns
    -------
    None
    """
    global log
    global window
    log.append(text)
    index = len(log) - 1
    window.Element("log").Update(
        values=log,
        set_to_index=index,
        scroll_to_index=index,
    )

def cancel_task() -> None:
    """
    Cancels the asyncio task responsible for reading the sc2bank file.

    Returns
    -------
    None
    """
    global task
    global twitch_task
    print(f"Cancelling Task: {task}, {twitch_task}")
    if isinstance(task, asyncio.Task):
        status = task.cancel()
        print(status)
    if isinstance(twitch_task, asyncio.Task):
        status = twitch_task.cancel()
        print(status)

def change_button_state(window: PySimpleGUI.Window) -> None:
    """
    Changes the buttons enabled state from the opposite of its defined
    state in the layout.

    PySimpleGUI handles this rather poorly.
    Parameters
    ----------
    window: :class: `PySimpleGUI.Window`
        The window to update the buttons in.

    Returns
    -------
    None
    """
    def change_state(attr, seq, check_str):
        for name in seq:
            element: PySimpleGUI.Element = window.Element(name)
            try:
                widget = element.__getattribute__(attr)
                new_status = not widget['state'] == check_str
                window.Element(name).Update(disabled=new_status)
            except AttributeError:
                print(f"{name} no attribute disabled")
                continue

    buttons = ['browse', 'stop', 'start', 'recover']
    change_state('TKButton', buttons, 'disabled')
    entries = ['discord_id',]
    change_state('TKEntry', entries, 'readonly')


async def main(loop=None):
    """
    The main loop for the GUI. This will update every 0.05 seconds to allow
    input while the loop is running.
    Returns
    -------

    """
    global window
    global log
    global task
    global twitch_task
    global chat_filename
    loop = loop if loop else asyncio.get_event_loop()
    while True:
        event, values = window.Read(timeout=0)

        if event in (None, 'Cancel'):
            break

        if event == 'draw':
            confirm = PySimpleGUI.PopupYesNo(match_draw_popup)
            if confirm == 'No' or confirm is None:
                continue
            payload = {
                'type': 'match_end',
                'time': LAST_EVENT.get('time') or 0,
                'game_id': settings['meta']['game_id'],
                'league': settings['meta'].get('league'),
                'season': settings['meta'].get('season'),
                'winning_team': '0,1,2,3'
            }

            await send_event(settings['meta']['uri'], json.dumps(payload))
            add_to_log("Sent DRAW Event. Restarting Listener...")
            cancel_task()
            twitch_task = loop.create_task(consumer())
            task = asyncio.get_event_loop().create_task(
                read_task(recover=False, recover_resume=False)
            )
            prepare_chat_xml()
            settings['meta']['twitch_chat_n'] = 0
            window.Element('draw').Update(disabled=True)

        if event == 'stop':
            confirm = PySimpleGUI.PopupYesNo(stop_listening_yesno_popup)
            if confirm == 'No' or confirm is None:
                continue
            cancel_task()
            change_button_state(window)
            add_to_log("Stopped listening for data...")


        if event == 'start' or event == 'recover':
            recover = event == 'recover'
            recover_resume = True
            if recover:
                recover_choice = PySimpleGUI.PopupYesNo(recovery_yesno_popup)
                if recover_choice is None:
                    print("recover_choice None")
                    continue
                print(f"Recovery Choice: {recover_choice}")
                recover_resume = recover_choice == "Yes"

            try:
                with open("settings.json", "w") as f:
                    add_to_log("Successfully tested write permissions")
            except PermissionError:
                add_to_log("The client must be in a folder you have write permissions for.")
                continue

            bank_dir = get_bank_path(window, values)
            discord_id = values.get('discord_id', '')
            settings['discord_id'] = values.get('discord_id', '')
            settings['bank_dir'] = bank_dir



            # Even though we don't pass the guard checks below, lets write out
            # the settings so state will always change even on fails.
            write_settings(settings)

            if not os.path.exists(bank_dir):
                PySimpleGUI.Popup(textwrap.dedent(
                    f"""
                    Invalid Bank Directory:
                    
                    {bank_dir}
                    """
                ))
                continue

            event_bank_file = f"{settings['bank_dir']}/{EVENT_BANK}"
            ce_bank_file = f"{settings['bank_dir']}/{CE_BANK}"
            chat_filename = "{0}/twitch.SC2Bank".format(settings['bank_dir'])
            try:
                with OpenWithoutLock(event_bank_file, 'r') as bank:
                    data = bank.read()
            except pywintypes.error as e:
                add_to_log(f"Warning, {EVENT_BANK} does not exist yet.")
                try:
                    with OpenWithoutLock(ce_bank_file, 'r') as ce:
                        _data = ce.read()
                except pywintypes.error as e:
                    add_to_log(f"Could not find CE bank. Possible wrong directory.")
                    continue

            if not discord_id:
                PySimpleGUI.Popup("Enter a valid discord id.")
                continue

            set_streamer(discord_id)
            add_to_log(f"Using Streamer ID: {discord_id}")
            uri = settings['meta']['uri']
            if not await test_connection(uri):
                add_to_log(f"Error: could not connect to {uri} Try entering your discord id again.")
                continue
            add_to_log(f"Connection Test successful!")
            settings['meta']['game_id'] = get_game_id()

            add_to_log("Starting Twitch Consumer")
            if not os.path.exists(chat_filename) or event == 'start':
                # Only overwrite if they start listening (non recovery)
                prepare_chat_xml()
                settings['meta']['twitch_chat_n'] = 0
            twitch_task = loop.create_task(consumer())

            change_button_state(window)
            task = asyncio.get_event_loop().create_task(
                read_task(recover=recover, recover_resume=recover_resume)
            )
            # Settings were good, persist them again with updated meta.
            write_settings(settings)
        await asyncio.sleep(0.05)
    cancel_task()
    window.Close()


async def read_task(recover=False, recover_resume=True):
    print(f"Read Task: Recover: {recover} Resume: {recover_resume}")
    while True:
        try:
            if not recover:
                add_to_log("Waiting for new game...")
                await check_for_new_game()
            else:
                if recover_resume:
                    add_to_log(f"Recovering and Resuming From Key #{settings['meta'].get('key', 0)}")
                else:
                    add_to_log(f"Bypassing New Game Checks and starting to write.")
            if os.path.exists(chat_filename):
                os.remove(chat_filename)

            await producer(recover=recover, recover_resume=recover_resume)
            recover = False
        except asyncio.CancelledError:
            print("Task cancelled.")
            break


LOCAL = False
settings = get_settings()
settings['version'] = APP_VERSION
EVENT_BANK = "Events.SC2BANK"
CE_BANK = 'ZoneControlCE.SC2BANK'
log = []
task = None
twitch_task = None
try:
    chat_filename = "{0}/twitch.SC2Bank".format(settings['bank_dir'])
except KeyError:
    chat_filename = ""


def print_status_info(info):
    total = info.get(u'total')
    downloaded = info.get(u'downloaded')
    status = info.get(u'status')
    print(downloaded, total, status)

if __name__ == "__main__":
    client = Client(
        ClientConfig(), refresh=True, progress_hooks=[print_status_info]
    )
    app_update = client.update_check(APP_NAME, APP_VERSION)
    if app_update is not None:
        app_update.download()
    if app_update is not None and app_update.is_downloaded():
        app_update.extract_restart()

    window_layout = layout(settings)
    window = PySimpleGUI.Window("ZCL OBS Recorder", window_layout).Finalize()
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(loop))



