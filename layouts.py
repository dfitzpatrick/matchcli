import PySimpleGUI
import textwrap
import typing


def layout(settings: typing.Dict[str, typing.Any]) -> typing.List[list]:
    return [
        [PySimpleGUI.Text("ZCL OBS Match Recorder")],
        [PySimpleGUI.Text(f"Version {settings.get('version')}")],
        [
            PySimpleGUI.Text("Discord ID"),
            PySimpleGUI.InputText(
                key='discord_id',
                default_text=settings.get("discord_id", '')
            )
         ],
        [PySimpleGUI.Text("Bank Location:", text_color='#01826B')],
        [
            PySimpleGUI.Text(
                settings.get("bank_dir", "Not Set"),
                size=(50,0),
                auto_size_text=True,
                key='bank_dir'
            ),
            PySimpleGUI.FolderBrowse(
                key='browse',
                initial_folder=settings.get("bank_file")
            )
        ],
        [
            PySimpleGUI.Button("Start Listening", key='start'),
            PySimpleGUI.Button("Stop Listening", key='stop', disabled=True),
            PySimpleGUI.Button("Recover", key='recover'),
            PySimpleGUI.Button("Send Draw", key='draw', disabled=True),
        ],
        [PySimpleGUI.Listbox(values=(), size=(100, 10), key='log')]
    ]

stop_listening_yesno_popup = textwrap.dedent(
    """
        WARNING! DO NOT DO THIS IF A GAME IS IN PROGRESS
    ----------------------------------------------------------------------------
    Stopping this now if a game is in progress will cause information 
    to not be written to our database. At the very best you'll need 
    to use the recovery feature. At the very worst, the data will be gone 
    forever.

    Basically:
        Are you in an active game? Click NO!

    DO YOU WANT TO STILL STOP LISTENING?
    """

)

recovery_yesno_popup = textwrap.dedent(
    """
        ARE YOU RESUMING FROM A CURRENT GAME?
    ----------------------------------------------------------------------------

    Yes:
        HIT YES IF YOU HIT STOP LISTENING OR THE PROGRAM CRASHED WHILE STREAMING

        The client will listen for the last written key to the recovery file.
        If you are in a new game this means that nothing will write. 

    No:
        HIT NO IF YOU FORGOT TO START THE LISTENER BEFORE THE GAME STARTED.

        The listener will force start reading the current game from the beginning.
        This bypasses checks for a new game. If a partial write has happened it 
        could duplicate events.

    """
)
match_draw_popup = textwrap.dedent(
    """
    This will cause the game to end in a draw. No further db writes will happen.
    WARNING! YOU CAN NOT UNDO THIS ACTION!
    ----------------------------------------------------------------------------
    
    Yes:
        HIT YES if you forgot to type -match_end in the game if the game was a draw
        or a remake.
    
    No:
        Do nothing. If you aren't sure what this does, this is what you should press.
    """
)